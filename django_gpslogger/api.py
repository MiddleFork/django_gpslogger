from django.conf.urls import url, include

from rest_framework.renderers import JSONRenderer
from rest_framework.routers import DefaultRouter
from rest_framework.serializers import ModelSerializer
from rest_framework.viewsets import ModelViewSet

from django_gpslogger.models import Reading


class ReadingSerializer(ModelSerializer):
    """Reading Serializer"""
    class Meta:
        model = Reading
        fields = '__all__'


class ReadingViewSet(ModelViewSet):
    model = Reading
    renderer_classes = (JSONRenderer, )
    serializer_class = ReadingSerializer
    queryset = Reading.objects.all()


router = DefaultRouter()
router.register(r'readings', ReadingViewSet)

urlpatterns = [
    url(r'^', include(router.urls))

]