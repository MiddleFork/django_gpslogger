from django.db import models


class Reading(models.Model):
    """Individual Location Reading"""
    deviceID = models.TextField()
    lat = models.FloatField()
    lon = models.FloatField()
    annotation = models.TextField(blank=True, null=True)
    satellites = models.IntegerField(blank=True, null=True)
    altitude = models.FloatField(blank=True, null=True)
    speed = models.FloatField(blank=True, null=True)
    accuracy = models.FloatField(blank=True, null=True)
    provider = models.TextField(blank=True, null=True)
    battery = models.FloatField(blank=True, null=True)
    activity = models.TextField(blank=True, null=True)
    timestring = models.TextField()
    timestamp = models.DateTimeField(blank=True, null=True)

    class Meta:
        db_table = 'readings'